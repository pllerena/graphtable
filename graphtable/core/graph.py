import numpy as np

from progressivis.table.dshape import (dshape_create, dshape_fields, dshape_to_shape, dshape_union)
from progressivis.table import metadata
from progressivis.table.table import Table

from progressivis.table.dshape import dshape_extract

from graphtable import util
import pandas as pd

from collections import Mapping

import matplotlib.pyplot as plt


import logging

logger = logging.getLogger(__name__)


class GTable(Table):
    def __init__(self, *args,  col_roles=None, **kwargs):
        super().__init__(*args, **kwargs)
        #init_col_roles
        if col_roles is not None:
            self._col_roles = col_roles
        else:
            self._col_roles = {}
            for col_name in self.columns:
                if col_name in self._col_roles:
                    self._col_roles[col_name] = col_name
    @property
    def col_roles(self):
        return self._col_roles

    def get_col(self, col_name):
        if col_name in self.col_roles:
            col_name = self.col_roles[col_name]
            if col_name in self.columns:
                return self.__getitem__(col_name)
        return None
        
    def _set_colum_data(self, data, column):
        column.resize(len(data))
        column[:] = data
        
    def set_col_roles(self, col_roles):
        for role, colname in col_roles.items():
            if colname not in self.columns:
                raise ValueError('Column name {} not in table'.format(colname))

        for role, colname in col_roles.items():
            if role in self._col_roles:
                logger.warning('overriding column role')
            self._col_roles[role] = colname
            
    def add_columns_from_nparray(self, data=None, col_names=None, names_roles={}):
        if data is not None:
            if self.nrow != len(data):
                raise ValueError("Incompatible length {}/{}".format(len(data), self.nrow))
            
            if data.ndim == 1:
                data = data[:, np.newaxis]
            dshape = dshape_extract(data.T, col_names)
            
            i = 0
            for (name, dshape) in dshape_fields(dshape_create(dshape)):
                
                col_data = data[:, i]
                col_shape = "{{{}: {}}}".format(name, dshape)
                col_role = names_roles[name] if name in names_roles else None

                self.add_column(col_shape, col_data, col_role)

                i+=1 
        
        
    def add_column(self, dshape, data=None, col_role=None):
        name, dshape_ = dshape_fields(dshape_create(dshape))[0]
        
        if name in self._columndict: return False
        
        if data is not None and self.nrow != len(data):
            raise ValueError("Incompatible length {}/{}".format(len(data), self.nrow))
        
        node = self.storagegroup
        node.attrs[metadata.ATTR_DATASHAPE] = dshape_union(self._dshape, dshape_create(dshape))
        self._dshape = dshape_create(node.attrs[metadata.ATTR_DATASHAPE])
        
        shape = dshape_to_shape(dshape_)

        fillvalue = None
        chunks = self._chunks_for(name)
        #TODO compute chunks according to the shape
        column = self._create_column(name)
        column.create_dataset(dshape=dshape_,
                              chunks=chunks,
                              fillvalue=fillvalue,
                              shape=shape)
        if data is not None:
            self._set_colum_data(data, column)
            
        if col_role is not None:
            self.set_col_roles({col_role: name})

        return True
        



class Nodes():
    def __init__(self, ids):
        if ids is not None:
            if isinstance(ids, Mapping):
                ids = np.array(ids)
            if isinstance(ids, np.ndarray):
                self.table = GTable('node_table', dshape="{id: int}", data=ids, col_roles={'id': 'id'})
        else:
            self.table = GTable('node_table', dshape="{id: int}", col_roles={'id': 'id'})

    def __getitem__(self, key):
        return self.table.get_col(key)


    def add_data_from_nparray(self, data, col_names=None, col_roles=None):
        self.table.add_columns_from_nparray(data, col_names, col_roles)

    def add_columns_from_function(self, function, col_names, col_roles=None, **kwargs):
        
        col_data = function(self, **kwargs)

        self.add_data_from_nparray(col_data, col_names, col_roles)
        
    def add_node(self, nid, data=None):
        if data is not None:
            data = util.flatten_dict(data)
            for k, v in data.items():
                self.table.add_column(dshape_extract({k: v}))
        else:
            data = {}
        data['id'] = [nid]
        
        self.table.append(data)
            
            
class Edges():
    def __init__(self, ids):
        if ids is not None:
            if isinstance(ids, Mapping):
                ids = np.array(ids)
            if isinstance(ids, np.ndarray):
                self.table = GTable('edge_table', dshape="{id_0: int, id_1: int}", data=ids,
                                    col_roles={'source': 'id_0', 'dest': 'id_1'})
        else:
            self.table = GTable('edge_table', dshape="{id_0: int, id_1: int}",
                                    col_roles={'source': 'id_0', 'dest': 'id_1'})

    def __getitem__(self, key):
        return self.table.get_col(key)

    def add_data_from_nparray(self, data, col_names=None, col_roles=None):
        self.table.add_columns_from_nparray(data, col_names, col_roles)

    def add_edge(self, source, target, data=None):
        if data is not None:
            data = util.flatten_dict(data)
            for k, v in data.items():
                self.table.add_column(dshape_extract({k: v}))
        else:
            data = {}

        data[self.table.col_roles['source']] = [source]
        data[self.table.col_roles['dest']] = [target]
        
        self.table.append(data)
            

class Graph():
    def __init__(self, node_ids=None, edges=None, node_b_ids=None):
        if node_ids is None:
            self._nodes = Nodes(node_ids)
        if node_b_ids is None:
            self._nodes_b = self._nodes
        else:
            self._nodes_b_ids = Nodes(node_b_ids)
        if edges is not None:
            self._edges = Edges(edges)

        self.initflags()

    def from_numpy_array(self, adj_matrix):
        n, m = adj_matrix.shape
        self._nodes = Nodes(np.arange(n))
        if n == m:
            self._nodes_b = self._nodes
        else:
            self._nodes_b = Nodes(np.arange(n))

        # get a list of edges
        x = np.array(adj_matrix.nonzero()).T
        self._edges = Edges(x)

        self.initflags()


    def initflags(self):
        self._node_pos_changed = True
        self._store_edges_pos = True
        
    @property
    def nodes(self):
        return self._nodes
    
    @property
    def nodes_b(self):
        return self._nodes_b
    
    @property
    def edges(self):
        return self._edges

    @property
    def number_of_edges(self):
        return len(self.edges.table)

    @property
    def number_of_nodes(self, which='source'):
        if which == 'source':
            return len(self.nodes.table)
        else:
            return len(self.nodes_b.table)

    
    def set_nodes(self, node_ids, data=None):
        self._nodes = Nodes(node_ids)
        #TODO: add data

    # def set_node_attributes(values, name=None, nodes=None):
        # self._nodes[name] = values

    def set_nodes_b(self, node_b_ids, data=None):
        self._nodes_b = Nodes(node_b_ids)
        #TODO: add data
        
    def add_node_a(self, nid, data=None):
        if self._nodes is self._nodes_b:
            logger.warning('node source table is the same as destination table')
        self._nodes.add_node(nid, data)
    
    def add_node_b(self, nid, data=None):
        if self._nodes is self._nodes_b:
            logger.warning('node source table is the same as destination table')
        self._nodes_b.add_node(nid, data)
    
    def add_node(self, nid, data=None):
        if self._nodes is not self._nodes_b:
            logger.warning('node source table is the same as destination table')
        self._nodes.add_node(nid, data)

    def add_edge(self, source, target, data=None):
        self._edges.add_edge(source, target, data)

    
    def add_node_attributes(self, attributes, names, roles, nodes=None):
        if nodes is None:
            nodes = self.nodes
        
        col_roles = {names[i]: roles[i] for i in range(len(roles))}

        if isinstance(attributes, np.ndarray):
            nodes.add_data_from_nparray(data=attributes, col_names=names, col_roles=col_roles)

        elif callable(attributes) or all([callable(fun_elem) for fun_elem in attributes]):
            nodes.add_columns_from_function(attributes, col_names=names, col_roles=col_roles)


    def add_node_attribute(self, attributes, name, role, nodes=None):
        if nodes is None:
            nodes = self.nodes
        
        self.add_node_attributes(attributes=attributes, names=[name], roles=[role])



    def draw_nodes(self, posx='_x', posy='_y', 
                        node_size=100,
                        node_color='#1f78b4',
                        node_shape='o',
                        alpha=1.0,
                        cmap=None,
                        vmin=None,
                        vmax=None,
                        ax=None, 
                        nodes=None,
                        **kwd): #add start end
        try:
            import matplotlib.pyplot as plt
            from matplotlib.colors import is_color_like
        except ImportError:
            raise ImportError("Matplotlib required for draw()")
        except RuntimeError:
            print("Matplotlib unable to open display")
            raise
            raise
        if nodes is None:
            nodes = self.nodes
        if ax is None:
            ax = plt.gca()
        if isinstance(node_color, str):
            if not is_color_like(node_color):
                node_color = nodes[node_color]
        node_collection = ax.scatter(nodes[posx], nodes[posy],
                                 s=node_size,
                                 c=node_color,
                                 marker=node_shape,
                                 cmap=cmap,
                                 vmin=vmin,
                                 vmax=vmax,
                                 alpha=alpha)
        node_collection.set_zorder(2)  # edges go behind nodes



    def draw_edges(self, posx='_x', posy='_y', 
                        width=1.0,
                        edge_color='grey',
                        style='solid',
                        alpha=1.0,
                        ax=None,
                        start=None,
                        end=None,
                        **kwd): #add start end
        try:
            import matplotlib.pyplot as plt
            from matplotlib.collections import LineCollection
            import numpy as np
        except ImportError:
            raise ImportError("Matplotlib required for draw()")
        except RuntimeError:
            print("Matplotlib unable to open display")
            raise
        if ax is None:
            ax = plt.gca()

        def compute_edge_pos(start, end):
            def in_range (i, start, end):  
                valid_start = True
                valid_end = True
                if start is not None:
                    valid_start =  int(self.edges['_start_time'][i]) >= start
                if valid_start and end is not None:
                    valid_end = int(self.edges['_end_time'][i]) < end
                return valid_start and valid_end
            
            edges_pos = []
            for i in range(self.number_of_edges):
                if in_range(i, start, end):
                    edges_pos.append([
                            [self.nodes['_x'][self.edges['source'][i]], self.nodes['_y'][self.edges['source'][i]]], 
                            [self.nodes['_x'][self.edges['dest'][i]], self.nodes['_y'][self.edges['dest'][i]]]
                        ])
            return edges_pos

        edges_pos = compute_edge_pos(start, end)

        ax = plt.gca()
        edge_collection = LineCollection(edges_pos,
                                        colors=edge_color,
                                         linewidths=width,
                                         antialiaseds=(1,),
                                         linestyle=style)

        edge_collection.set_zorder(1)  # edges go behind nodes
        ax.add_collection(edge_collection)

        # if self._node_pos_changed && self._store_edges_pos:


    def draw(self, posx='_x', posy='_y', **kwds):
        self.draw_nodes(posx, posy, ax=None, **kwds)
        if self.nodes != self.nodes_b:
            self.draw_nodes(posx, posy, nodes=nodes_b, **kwds)
        self.draw_edges(posx, posy, **kwds)


    def draw_dynamic(self, posx='_x', posy='_y', ax=None):
        if ax is None:
            ax = plt.gca()
        self.draw_nodes(posx, posy, ax=ax)



