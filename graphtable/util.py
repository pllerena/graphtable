from collections import OrderedDict, Mapping


def flatten_dict(data, sep='_'):

    queue = [('', data)]

    data_parsed = {}

    while len(queue) > 0:

        elem = queue.pop()
        elem_key = elem[0]
        elem_data = elem[1]

        for k, val in elem_data.items():
            # curr_key = k if len(elem_key) == 0 else elem_key + sep + k 
            curr_key = elem_key + sep + k 
            if isinstance(val, Mapping):
                queue.append((curr_key, val))
            else:
                data_parsed[curr_key] = [val]
    return data_parsed



