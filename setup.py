from distutils.core import setup

setup(name='graphtable',
      version='0.0.1',
      packages=['graphtable', 'graphtable.core', 'graphtable.io'],
      )
